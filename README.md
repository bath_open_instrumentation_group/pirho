# PiRho is an open source spectroscopic pyrometer

The pyrometer is designed to be cheap and simple to make for calibrating furnaces in low resource settings.

Currently the pyrometer consists of a box-section (an old table leg), some 3D printed parts, and a raspberry Pi and pi-camera.

The 3D printed parts at the front end need to be replaced (probably by welding Stanley blades to the front), and the pi and
pi-camera are way too over-specced for their job, but they were available and easy to use. They will be replaced by
appropriate alternatives once the first version works.

## Working principle
![](Ims/Schematic.png)