from picamera import PiCamera
import picamera.array
from PIL import Image
from matplotlib import pyplot as plt
import numpy as np
import time
from fractions import Fraction
import copy 
import os

class pyrometer():
    
    def __init__(self,calib=None):
        self.cam = PiCamera()
        self.cam.resolution = 1024,2464
        self.cam.framerate = Fraction(5,1)
        self.cam.exposure_mode = 'off'
        self.cam.iso = 100
        self.cam.awb_mode = 'off'
        self.cam.awb_gains = (1,1)
        self.cam.image_denoise = False
        self.cam.shutter_speed = 100000
        time.sleep(.1)
        img = picamera.array.PiRGBArray(self.cam)
        self.cam.capture(img,'rgb')
        self.cam.exposure_mode = 'auto'
        self.cam.exposure_mode = 'off'
        self.cam.iso = 100
        self.calib = calib
        if self.calib is None and os.path.exists(os.path.expanduser('~/.pyro_rc')):
            with open(os.path.expanduser('~/.pyro_rc'),'r') as file:
                cal = yaml.load(file)
                self.calib = calibration(cal['pixel'],cal['wavelength'],cal['coef'])

    def set_shutter_speed(self,speed):
        self.cam.shutter_speed = speed
        time.sleep(.1)
    
    def get_shutter_speed(self):
        return self.cam.shutter_speed


    def capture(self):
        img = picamera.array.PiRGBArray(self.cam)
        self.cam.capture(img,'rgb')
        t = time.localtime()
        r = img.array[:,:,0].astype(float)
        g = img.array[:,:,1].astype(float)
        b = img.array[:,:,2].astype(float)
        
        #TODO: 1700 is arbitrary, should be set by calibration if available
        a = r[0:1700]+2*g[0:1700]+b[0:1700]
        #find maxima in each row, return the average as the column
        col = int(np.mean([i for i in np.argmax(a,1) if not i == 0]))
        # average values around that column
        r = np.flipud(np.mean(r[:,col-5:col+5],1))
        g = np.flipud(np.mean(g[:,col-5:col+5],1))
        b = np.flipud(np.mean(b[:,col-5:col+5],1))
        
        return reading((r,g,b),img,t,col,calib)
        
    def preview(self):
        self.cam.start_preview()
        tmp = input()
        self.cam.stop_preview()

class reading():
    
    def __init__(self,rgb,img,t,col,calib=None):
        self.r = rgb[0]
        self.g = rgb[1]
        self.b = rgb[2]
        self.coef = [1,2,1]
        self.spec = self.cal_spec()
        self.srange = [len(self.r)-1700,len(self.r)]
        self.img = img
        self.time = t
        self.col = col
        self.calib = calib
        
    def cal_spec(self):
        return self.r*self.coef[0]+self.g*self.coef[1]+self.b*self.coef[2]
    
    def findpeak(self,p_size = 2):
        #p_size sets the peak size:
        # 1: peak point must be higher than nearest neighbours
        # 2: Peak point must be higher than its nearest neigbours. These neighbours must be higher than their outer neibours
        P = np.ones(len(self.spec),dtype=bool)
        for a in range(p_size):
            P &=  np.hstack([np.zeros(a+1,dtype=bool), self.spec[1:len(self.spec)-a] > self.spec[:-1-a]]) & np.hstack([self.spec[a:-1] > self.spec[1+a:], np.zeros(a+1,dtype=bool)])
        return np.where(P)[0]
    
    def assign_peaks(self,P=None):
        
        
        def sanitise(inp):
            if inp is not None:
                try:
                    inp=float(inp)
                except ValueError:
                    inp=None
            return inp
    
        if P is None:
            P = self.findpeak()
        x = np.asarray(range(self.srange[0],self.srange[1]))
        P = P[P<self.srange[1] ]
        P = P[P>self.srange[0] ]
        peakPairs=[]
        for p in P:
            plt.figure(1)
            plt.clf()
            plt.plot(x,self.spec[x],'k-')
            plt.plot(p,self.spec[p],'g.')
            plt.show(block=False)
            inp = sanitise(input('Enter peak value for green peak:'))
            if inp is not None: 
                peakPairs.append([p,inp])
        plt.close()
        px = np.asarray([p[0] for p in peakPairs])
        wl = np.asarray([p[1] for p in peakPairs])
        self.calib = calibration(px,wl)
        

    def show_im(self):
        plt.imshow(self.img.array)
        plt.show()
        
    def show_spec(self,allchannels=False):
        x = range(len(self.spec))
        if self.calib is not None:
            x=self.calib.calibrate(x)
            xmin = np.argmin(np.abs(x-350))
            xmax = np.argmin(np.abs(x-850))
            xlab = 'Wavelength (nm)'
        else:
            xmin = 0
            xmax = len(self.spec)
            xlab = 'Pixel number'
        if allchannels:
            plt.plot(x[xmin:xmax],self.r[xmin:xmax],'r-')
            plt.plot(x[xmin:xmax],self.g[xmin:xmax],'g-')
            plt.plot(x[xmin:xmax],self.b[xmin:xmax],'b-')
        plt.plot(x[xmin:xmax],self.spec[xmin:xmax],'k-')
        plt.ylabel('Intensity (arb units)')
        plt.xlabel(xlab)
        plt.show()
    
    def save(self,filename=None,raw=False):
        
        if raw:
            data = np.vstack([self.r,self.g,self.b]).transpose()
            fileident = 'RAW'
        else:
            data = self.spec
            fileident = 'Spectrum'
            
        if filename is None:
            filename = time.strftime('%Y%m%d-%H%M%S',self.time)+'_'+fileident+'.dat'
        np.savetxt(filename,data)
        
    def save_im(self,filename=None):
        im = Image.fromarray(self.img.array)
        
        if filename is None:
            filename = time.strftime('%Y%m%d-%H%M%S',self.time)+'_Image.png'
        im.save(filename)

class calibration():
    
    def __init__(self,pixel,wavelength,coef=None):
        self.pixel = pixel
        self.wavelength=wavelength
        if coef is None:
            self.coef = np.polyfit(self.pixel,self.wavelength,1)
        else:
            self.coef = coef
        
    def calibrate(self,px):
        return px*self.coef[0]+self.coef[1]
    
    def save(self,location=None):
        with open(os.path.expanduser('~/.pyro_rc'),'w') as file:
            file.write(yaml.dump({'pixel':self.pixel,'wavelength':self.wavelength,'coef':self.coef}))

        

if __name__ == "__main__":
    
    Pyro = pyrometer()
